module.exports = mongoose.model('Transactions', transactionSchema)
const Transaction = require('../model/transactionsModel.js')

exports.showTransaction = function(req, res) {
        Transaction.find({id :req.transaction},(err,transaction)=> {
                err ? res.send(err) : res.json(transaction)
        })

}