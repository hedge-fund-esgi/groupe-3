const jwt = require('jsonwebtoken');
const User = require('../models/userModel');
const sha256 = require('sha256');
const config = require('../config/secrets');

exports.user_signup = function(req,res){
    var new_user = new User(req.body);
    if(new_user.password != req.body.password_confirmation){
        res.json("Password Confirmation does not match the password");
    }else{
        var user_exists = User.findOne({email: req.body.email}, function(err, user){
        if(err) res.send(err);
        
        if (user === null) {
            console.log(sha256(req.body.password));
            new_user.password = sha256(req.body.password)
            new_user.lastname = req.body.lastname
            new_user.username = req.body.username
            new_user.firstname = req.body.firstname
            new_user.save(function(err, user){
                if(err) return res.send(err);
                else return res.json(user);
            });
        }else {
            res.json("User with given email already exists");
        }
        });
    }
};

exports.get_users = function(req, res){
    User.find({}, function(err, users) {
        if(err) return res.status(500).send(err);
        var userMap = {};
    
        users.forEach(function(user) {
          userMap[user._id] = user;
        });
    
        res.json(userMap);
      });
  };

exports.user_me = function(req, res){
    User.findOne({email: req.query.email}, function(err,user){
        if(err) return res.status(500).send(err);
        console.log(req.query.email);
        return res.json(user);
    })
}

exports.user_auth = function(req, res){
    console.log("user_auth");
    User.findOne({email: req.body.username}, function(err, user){
        console.log("find one");
        if(err) return res.status(500).send(err);
        if (user != null) {
            if(user.password == sha256(req.body.password)){
                console.log('password correct');
                jwt.sign({user}, config.secrets.jwt_key, {expiresIn: '30 days'}, (err, token) => {
                    if(err){
                        return res.json({
                        success: false,
                        message: 'Something went wrong'
                        });
                    }else {
                        console.log("else");
                        console.log(token);
                        res.setHeader('auth_token', token);
                        console.log(token);
                        res.status(200);
                        return res.json({
                            success: true,
                            message: user,
                            token: token
                        });
                    }
                })
            }else{
                console.log("password incorrect");
                return res.json({
                    success: false,
                    message: 'Password don\'t match'
                });
            }
        } else {
            console.log("user incorrect");
            return res.json({
                success: false,
                message: 'Incorrect user'
            });
        }
    });
};

exports.user_update = function(req, res){
    User.findOneAndUpdate({email: req.query.email}, req.body, {new: true}, (err,done)=>{
      if(err) return res.status(500).send(err);
      return res.send(done);
    });
};
  
exports.user_delete = function(req, res){
    User.findOneAndDelete({email: req.body.email}, (err, done)=>{
        if(err) return res.statust(500).send(err);
        return res.send(done);
    })
};