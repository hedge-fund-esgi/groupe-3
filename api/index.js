const express = require('express');
const app = express();
const mongoose = require('mongoose');
const bodyParser = require('body-parser');


var connectWithRetry = function() {
  return mongoose.connect("mongodb://mongo_db:27017/api", function(err) {
    if (err) {
      console.error('Failed to connect to mongo on startup - retrying in 5 sec', err);
      setTimeout(connectWithRetry, 5000);
    }else{
        console.log('Connected to mongo');
    }
  });
};
connectWithRetry();

//mongoose.connect('mongodb://mongo_db:27017/api', {useNewUrlParser: true});

const routes = require('./routes/route.js');
const userRoute = require('./routes/userRoute');
const sharesRoute = require('./routes/sharesRoute');

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
  next();
});

app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());


routes(app);
userRoute(app);
sharesRoute(app);

// app.get('/', function (req, res) {
//   res.send('Hello World!')
// })

const hostname = process.env.API_HOST;
const port = process.env.API_PORT;

app.listen(port);

console.log("Running on " , port);
