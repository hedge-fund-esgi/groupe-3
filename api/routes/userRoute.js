module.exports = function(app){
    const user = require('../controllers/userController');
    const jwtMiddleware = require('../middleware/jwtMiddleware');

    app.post('/signup', user.user_signup);
    app.get('/getusers', user.get_users);
    app.get('/myuser', user.user_me);
    app.post('/auth', user.user_auth);
    app.patch('/updateuser', jwtMiddleware.verify_token ,user.user_update);
    app.delete('/deleteuser',jwtMiddleware.verify_token, user.user_delete);
};