const mongoose = require('mongoose');

var Schema = mongoose.Schema;

var userSchema = new Schema({
    email: {
        type: String,
        required: true,
        unique: true
    },
    password: {
        type: String,
        required: true
    },
    firstname: {
      type: String
    },
    lastname: {
      type: String
    },
    username: {
      type: String
    }
});


module.exports = mongoose.model('User', userSchema);