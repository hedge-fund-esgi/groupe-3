const mongoose = require('mongoose')

const Schema = mongoose.Schema
<<<<<<< HEAD

const transactionSchema = new Schema({
        // Id of the transaction
        id: {
                type: String,
                required: true,
                unique : true
        },
        // Maker of the transaction
        user: {
                type: String,
                required: true,
                unique : true
        },
        // Address of the transaction
        address: {
                type: String,
                required: true,
                unique: true
        },
        // Buy or Sell
        type: {
                type: String
        },
        // Token of the transaction
        token: {
                type: String
        },
        // Date of the transaction
        dateOfTransaction: {
                type: Date
        }
})

module.exports = mongoose.model('Transaction', transactionSchema);
=======
const transactionSchema = new Schema({
        id: {
                type: String,
                required: 'Transaction ID is required'
        },
        address: {
                type: String,
                required: 'Address of the transaction is required'
        },
        type: {
                type: String
        },
        details: {
                type: String
        },
        dateOfTransaction: {
                type: Date
        }
})
>>>>>>> 0468017453d308c836972c498ec7312a773ae408
