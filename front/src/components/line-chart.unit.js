import lineChart from './line-chart'

describe('@components/line-chart', () => {
  it('exports a valid component', () => {
    expect(lineChart).toBeAComponent()
  })
})
