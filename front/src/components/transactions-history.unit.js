import TransactionsHistory from './transactions-history'

describe('@components/transactions-history', () => {
  it('exports a valid component', () => {
    expect(TransactionsHistory).toBeAComponent()
  })
})
