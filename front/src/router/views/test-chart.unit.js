import TestChart from './test-chart'

describe('@views/test-chart', () => {
  it('is a valid view', () => {
    expect(TestChart).toBeAViewComponent()
  })
})
